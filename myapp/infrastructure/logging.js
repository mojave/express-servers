export function log(msg){
    const dateString = new Date().toDateString();
    const timeString = new Date().toLocaleTimeString();
    const timestamp = dateString + ' - ' + timeString;
    console.log(`[${timestamp}]: ${msg}`);
}