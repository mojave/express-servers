const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const foodDb = require('./data-store/foodItems.js');
import { log } from './infrastructure/logging.js';



// middleware
app.use(bodyParser.json());

app.use( function (req, res, next) {
	// host you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', '*');
	// request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, PATCH');
	// request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	// pass to next layer of middleware
	next();
})

app.get('/', (req, res) => {
    log('GET request to \'/\'');
    res.send('Hello World!');
});

app.get('/foodItems', (req, res) => {
	log('GET reqeust to /foodItems')
	let foodItems = foodDb.getFoodItems();
	res.send(foodItems)
})

app.get('/foodItems/:foodId', (req, res) => {
	log('GET reqeust to /foodItems/' + req.params.foodId);
	let foodItem = retrieveFoodItem(req.params.foodId);
	res.send(foodItem)
})

app.post('/foodItems', function(req, res){
	log('POST request to /foodItems with: ' + req.body);
	postFoodItem(req.body)
	res.send("ok");
});

function retrieveFoodItem(foodId){
	return foodDb.getFoodById(foodId);
}

function postFoodItem(foodItem){
	return foodDb.postFoodItem(foodItem);
}

app.listen(3003, () => console.log('Example app listening on port 3003!'))
