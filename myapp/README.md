## setup/run
1. clone the express-servers project.
2. cd into the project directory (express-servers/myapp)
3. do `npm install`
4. then, for development, run `npm start`
 * npm start will kick off the express server with nodemon and babel.