var fs = require('fs');

module.exports = {

	getFoodItems: function(){
		var foodsString = fs.readFileSync('./data-store/foodItems.json','utf8');
		var foodsObj = JSON.parse(foodsString);
		return foodsObj;
	},

	getFoodById: function(id){
		const result = this.findItemById(id);
		return result;
	},

	putFoodItem: function(foodItem){
		var foodsObj = this.getFoodItems();
		// find the index of the desired item
		var indexOfItemToReplace = this.findItemIndex(foodItem.id)
		// then replace that item with
		foodsObj.splice(indexOfItemToReplace, 1, foodItem);
		this._writeToFile(foodsObj)
	},

	postFoodItem: function(foodItem){
		var foodsObj = this.getFoodItems();
		foodsObj.push(foodItem);
		this._writeToFile(foodsObj)
	},

	findItemById: function(id){
		var foodsObj = this.getFoodItems();
		const result = foodsObj.find( fruit => fruit.id == id );
		return result;
	},

	findItemIndex: function(id){
		var foodsObj = this.getFoodItems();
		const itemIndex = foodsObj.findIndex( fruit => fruit.id == id );
		return itemIndex;
	},

	_writeToFile: function(foods){
		console.log('writing foods to db: ' + JSON.stringify(foods))
		fs.writeFile('./data-store/foodItems.json', JSON.stringify(foods), 'utf8', function (err) {
			if (err) throw err;
			console.log('updated foodItems.json');
		});
	}
}
